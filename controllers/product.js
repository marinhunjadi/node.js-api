//const jwt = require('jwt-simple');
const Product = require('../models/product');
//const Role = require('../models/role');
//const config = require('../config');

// Handle index actions
exports.index = function (req, res) {
    Product.find(function (err, products) {
        if (err) return next(err);
        res.send(products);
    });
};

exports.seedProducts = function(req, res) {
  // create some products
  const products = [
    { name: 'eins' },
    { name: 'zwei' },
    { name: 'drei' },
    { name: 'vier' }
  ];

  // use the Event model to insert/save
  Product.remove({}, function(err, removed) {
	if (err) return next(err);  
    for (product of products) {
      var newProduct = new Product(product);
      newProduct.save();
    }
  });
  
  /*Role.remove({}, function(err, removed) {
	if (err) return next(err);  
    var newRole = new Role({ role: 'ADMIN' });
    newRole.save();
  });*/  

  // seeded!
  res.send('Database seeded!');
}

/*function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}*/

/*exports.signin = function(req, res, next) {
  // User has already had their email and password
  // We just need to give them a token
  res.send({ token: tokenForUser(req.user) });
}*/

/*exports.signup = function(req, res, next) {
  const email = req.body.email;
  const password = req.body.password;

  if (!email || !password) {
    return res.status(422).send({ error: 'You must provide email and password'});
  }

  // See if a user with the given email exists
  User.findOne({ email: email }, function(err, existingUser) {
    if (err) { return next(err); }

    // If a user with email does exist, return an error
    if (existingUser) {
      return res.status(422).send({ error: 'Email is in use' });
    }

    // If a user with email does NOT exist, create and save user record
    const user = new User({
      email: email,
      password: password
    });

    user.save(function(err) {
      if (err) { return next(err); }

      // Respond to request indicating the user was created
      res.json({ token: tokenForUser(user) });
    });
  });
}*/
